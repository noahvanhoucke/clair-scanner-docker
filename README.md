# Clair Scanner Docker Container

This project builds a docker container to run the clair-scanner. We use it as a part of
our CI/CD pipeline so that we can check security vulnerabilities before we go as far
as deploying code to our production environment. In this way we can make sure that new
vulnerabilities that appear don't get into our live systems.

The image works in conjunction with CoreOS clair <https://github.com/coreos/clair> and is based on some very nice work from <https://github.com/arminc/clair-scanner>

Most of what is done here comes from very useful information in an issue raised against the project; so many thanks go to the people in that discussion: <https://github.com/arminc/clair-scanner/issues/42>.
# Build

To build locally run:

```
docker build -t . clair-scanner
```
