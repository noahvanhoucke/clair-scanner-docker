FROM golang:1.9
RUN go get -u github.com/golang/dep/cmd/dep
RUN git clone https://github.com/arminc/clair-scanner.git src/clair-scanner/
RUN cd src/clair-scanner/ && make ensure && make build
RUN ls -l src/clair-scanner
RUN cp src/clair-scanner/clair-scanner /usr/local/bin
COPY ./install-docker.sh /opt/
RUN sh /opt/install-docker.sh
ENV DOCKER_API_VERSION=1.34
EXPOSE 9279

